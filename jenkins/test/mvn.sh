
#!/bin/bash

echo "****************************************"
echo "** Testing ***********************"
echo "****************************************"

WORKSPACE=/Users/zarrarmalik/documents/jenkins/jenkins_new/jenkins_home/workspace/pipeline-docker-maven

docker run --rm -v $WORKSPACE/xsd:/app -v /root/.m2/ -w /app maven:3-alpine "$@"

