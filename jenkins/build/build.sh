#!/bin/bash

# Copy the new jar to the build location
cp -f xsd/target/*.jar jenkins/build/

echo "*****************************"
echo "***** Building docker images ****************"
echo "**************************************"

cd jenkins/build/ && docker-compose -f docker-compose-build.yml build --no-cache
