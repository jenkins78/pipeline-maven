#!/bin/bash

echo "*******************************"
echo "************pushing image******"
echo "*******************************"

IMAGE="test-project"
echo "**** Loggin in ***********"
docker login -u zarrarmalik -p $PASS
echo "**** Tagging image ******"
docker tag $IMAGE:$BUILD_TAG zarrarmalik/$IMAGE:$BUILD_TAG
echo "********* Pushing image ********"
docker push zarrarmalik/$IMAGE:$BUILD_TAG
